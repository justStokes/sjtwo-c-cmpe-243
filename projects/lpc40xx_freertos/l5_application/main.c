/* standard includes */
#include <stdio.h>

/* FreeRTOS Includes */
#include "FreeRTOS.h"
#include "task.h"

/* Module Includes */
#include "periodic_scheduler.h"
#include "sj2_cli.h"

int main(void) {

  puts("Starting RTOS");
  periodic_scheduler__initialize();
  vTaskStartScheduler();

  return 0;
}